﻿using System.ComponentModel.DataAnnotations.Schema;

namespace sbAdminTCC.Dominio.Entidade
{
    public class FormaPagto
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public string Descricao { get; set; }
    }
}
