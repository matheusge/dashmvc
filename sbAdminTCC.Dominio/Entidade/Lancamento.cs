﻿using System;

namespace sbAdminTCC.Dominio.Entidade
{
    public class Lancamento
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public decimal Valor { get; set; }
        public DateTime DataLancamento { get; set; }
        public bool Realizado { get; set; }
        public DateTime? DataRealizado { get; set; }
        public int TipoAtividadeId { get; set; }
        public int FormaPagtoId { get; set; }
        public int ContaOrigemId { get; set; }
        public int CategoriaId { get; set; }
        public string UserId { get; set; }
    }
}
