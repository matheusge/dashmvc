﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace sbAdminTCC.Dominio.Entidade
{
    public class Categoria
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public string Descricao { get; set; }
        public int TipoAtividadeId { get; set; }
    }
}
