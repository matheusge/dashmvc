﻿using System;

namespace sbAdminTCC.Dominio.Entidade
{
    public class Conta
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public string UserId { get; set; }
    }
}
