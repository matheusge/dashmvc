﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sbAdminTCC.UI.Web.Models
{
    public class ComparativoMesCorrenteAnteriorViewModel
    {
        public ComparativoMesCorrenteAnteriorViewModel()
        {
            Meses = new List<string>();
            TotalDespFixa = new List<decimal>();
            TotalDespVariavel = new List<decimal>();
            TotalImposto = new List<decimal>();
            TotalRecebimento = new List<decimal>();
        }

        public IList<string> Meses { get; set; }
        public IList<decimal> TotalDespFixa { get; set; }
        public IList<decimal> TotalDespVariavel { get; set; }
        public IList<decimal> TotalImposto { get; set; }
        public IList<decimal> TotalRecebimento { get; set; }

        public int MesAnterior { get; set; }
    }

    public class ComparativoMesPassadoViewModel
    {
        public int MesAtual { get; set; }
        public string MesAtualAno { get; set; }
        public decimal TotalDespFixasMesAtual { get; set; }
        public decimal TotalDespVariaveisMesAtual { get; set; }
        public decimal TotalImpostosMesAtual { get; set; }

        public decimal TotalRecebimentosMesAtual { get; set; }

        public int MesAnterior { get; set; }
        public string MesAnteriorAno { get; set; }
        public decimal TotalDespFixasMesAnterior { get; set; }
        public decimal TotalDespVariaveisMesAnterior { get; set; }
        public decimal TotalImpostosMesAnterior { get; set; }
        public decimal TotalRecebimentosMesAnterior { get; set; }
    }
}