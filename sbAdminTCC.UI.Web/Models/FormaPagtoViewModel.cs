﻿using System.ComponentModel;

namespace sbAdminTCC.UI.Web.Models
{
    public class FormaPagtoViewModel
    {
        public int Id { get; set; }

        [DisplayName("Descrição")]
        public string Descricao { get; set; }
    }

}