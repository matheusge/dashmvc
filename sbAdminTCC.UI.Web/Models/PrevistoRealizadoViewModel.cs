﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sbAdminTCC.UI.Web.Models
{
    public class PrevistoRealizadoViewModel
    {
        public PrevistoRealizadoViewModel()
        {
            Percentual = 0;
        }

        public string Nome { get; set; }
        public decimal Percentual { get; set; }
    }
}