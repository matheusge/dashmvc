﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace sbAdminTCC.UI.Web.Models
{
    public class ContaViewModel
    {

        public int Id { get; set; }

        [DisplayName("Descrição")]
        public string Descricao { get; set; }
    }
}