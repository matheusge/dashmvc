﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sbAdminTCC.UI.Web.Models
{
    public class DREViewModel
    {
        public decimal ReceitaBruta { get; set; }
        public decimal Impostos { get; set; }
        public decimal LucroBruto { get; set; }
        public decimal TotalDespesasVariaveis { get; set; }
        public decimal LucroOperacional { get; set; }
        public decimal TotalDespesasFixas { get; set; }
        public decimal LiquidoDoExercicio { get; set; }
    }
}