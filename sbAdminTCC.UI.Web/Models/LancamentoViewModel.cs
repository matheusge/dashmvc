﻿using System;
using System.ComponentModel.DataAnnotations;
using sbAdminTCC.Dominio.Entidade;

namespace sbAdminTCC.UI.Web.Models
{
    public class LancamentoViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Descrição")]
        public string Descricao { get; set; }

        public decimal Valor { get; set; }

        [Display(Name = "Lançamento")]
        public DateTime DataLancamento { get; set; }

        public bool Realizado { get; set; }

        [Display(Name = "Realizado em")]
        public DateTime? DataRealizado { get; set; }

        public int ContaOrigemId { get; set; }

        [Display(Name = "Conta Origem")]
        public string ContaOrigemDescricao { get; set; }

        //[Display(Name = "Destino")]
        //public int? ContaDestinoId { get; set; }

        //[Display(Name = "Descrição")]
        //public string ContaDestinoDescricao { get; set; }

        public int FormaPagtoId { get; set; }

        [Display(Name = "Forma de Pagamento")]
        public string FormaPagtoDescricao { get; set; }
       
        //[Display(Name = "Tipo")]
        //public TipoAtividadeViewModel TipoAtividade { get; set; }

        [Display(Name = "Categoria")]
        public string CategoriaDescricao { get; set; }
        public int CategoriaId { get; set; }

        public int TipoAtividadeId { get; set; }
        public string DataLancamentoJson { get; set; }
        public string DataRealizadoJson { get; set; }
        public string ValorJson { get; set; }
    }

    public class LancamentoViewModelEdit
    {
        public int IdEdit { get; set; }
        public string DescricaoEdit { get; set; }
        public decimal ValorEdit { get; set; }
        public bool RealizadoEdit { get; set; }
        public int ContaOrigemIdEdit { get; set; }
        public int FormaPagtoIdEdit { get; set; }
        public int TipoAtividadeIdEdit { get; set; }
        public string DataLancamentoEdit { get; set; }
        public string DataRealizadoEdit { get; set; }
        public int CategoriaIdEdit { get; set; }
    }
}