﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(sbAdminTCC.UI.Web.Startup))]
namespace sbAdminTCC.UI.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
