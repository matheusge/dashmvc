﻿using System.Web.Mvc;

namespace sbAdminTCC.UI.Web.Controllers
{
    [Authorize]
    public class BaseController : AccountController
    {
        public string GetUser
        {
            get { return (string) Session["UserId"]; }
        }
    }
}