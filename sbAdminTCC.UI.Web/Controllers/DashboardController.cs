﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.UI.WebControls.WebParts;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using sbAdminTCC.Aplicacao.App;
using sbAdminTCC.Aplicacao.Fabrica;
using sbAdminTCC.Fabrica;
using sbAdminTCC.RepositorioEF.Repositorio;
using sbAdminTCC.UI.Web.Models;
using WebGrease.Css.Extensions;
using System.Text;
using System.Web.Script.Serialization;

namespace sbAdminTCC.UI.Web.Controllers
{
    [CustomAuthorize]
    public class DashboardController : BaseController
    {
        private readonly FormaPagtoAplicacao _appFormaPagto;
        private readonly ContaAplicacao _appConta;
        private readonly LancamentoAplicacao _appLancamento;
        private readonly CategoriaAplicacao _appCategoria;

        public DashboardController()
        {
            _appFormaPagto = FabricaAplicacao.GetNewFormaPagtoAplicacao();
            _appConta = FabricaAplicacao.GetNewContaAplicacao();
            _appLancamento = FabricaAplicacao.GetNewLancamentoAplicacao();
            _appCategoria = FabricaAplicacao.GetNewCategoriaAplicacao();
        }
        public ActionResult Index()
        {
            ViewBag.TitlePanelDRE = DateTime.Now.ToLongDateString();
            return View();
        }
        [HttpGet]
        public PartialViewResult LoadDREMesPartial()
        {
            return PartialView("_PartialDRE", this.LoadDREMes());
        }
        public DREViewModel LoadDREMes()
        {
            var oDRE = new DREViewModel
            {
                Impostos = 0,
                LiquidoDoExercicio = 0,
                LucroBruto = 0,
                LucroOperacional = 0,
                ReceitaBruta = 0,
                TotalDespesasFixas = 0,
                TotalDespesasVariaveis = 0
            };


            var lancamentos = _appLancamento.ListarTodosRealizadosByUserIdAndMonth(GetUser, DateTime.Now.Month).ToList();

            //RECEBIMENTOS
            //var lRecebimentosMes = _appLancamento.ListarTodosRealizadosByUserIdTipoAtivideIdMonth(GetUser, (int)GenericEnum.TipoAtividade.Recebimentos, DateTime.Now.Month).ToList();
            var lRecebimentosMes = lancamentos.Where(x => x.TipoAtividadeId == (int) GenericEnum.TipoAtividade.Recebimentos).ToList();
            if (lRecebimentosMes.Any())
            {
                var firstOrDefault = lRecebimentosMes.GroupBy(x => x.DataLancamento.Month).Select(y => new { TotalReceitaBruta = y.Sum(i => i.Valor) }).FirstOrDefault();
                oDRE.ReceitaBruta = firstOrDefault != null ? firstOrDefault.TotalReceitaBruta : 0;
            }

            //IMPOSTOS
            //var lImpostos = _appLancamento.ListarTodosRealizadosByUserIdTipoAtivideIdMonth(GetUser, (int)GenericEnum.TipoAtividade.Impostos, DateTime.Now.Month).ToList();
            var lImpostos = lancamentos.Where(x => x.TipoAtividadeId == (int)GenericEnum.TipoAtividade.Impostos).ToList();
            if (lImpostos.Any())
            {
                var firstOrDefault = lImpostos.GroupBy(x => x.DataLancamento.Month).Select(y => new { TotalImpostos = y.Sum(i => i.Valor) }).FirstOrDefault();
                oDRE.Impostos = firstOrDefault != null ? firstOrDefault.TotalImpostos : 0;
            }

            //CALCULO LUCRO BRUTO
            oDRE.LucroBruto = oDRE.ReceitaBruta - oDRE.Impostos;

            //DESPESAS VARIÁVEIS
            //var lDespVariaveis = _appLancamento.ListarTodosRealizadosByUserIdTipoAtivideIdMonth(GetUser, (int)GenericEnum.TipoAtividade.DespesasVariaveis, DateTime.Now.Month).ToList();
            var lDespVariaveis = lancamentos.Where(x => x.TipoAtividadeId == (int)GenericEnum.TipoAtividade.DespesasVariaveis).ToList();
            if (lDespVariaveis.Any())
            {
                var firstOrDefault = lDespVariaveis.GroupBy(x => x.DataLancamento.Month).Select(y => new { TotalDespVariaveis = y.Sum(i => i.Valor) }).FirstOrDefault();
                oDRE.TotalDespesasVariaveis = firstOrDefault != null ? firstOrDefault.TotalDespVariaveis : 0;
            }

            //DESPESAS FIXAS
            //var lDespesasFixas = _appLancamento.ListarTodosRealizadosByUserIdTipoAtivideIdMonth(GetUser, (int)GenericEnum.TipoAtividade.DespesasFixas, DateTime.Now.Month).ToList();
            var lDespesasFixas = lancamentos.Where(x => x.TipoAtividadeId == (int)GenericEnum.TipoAtividade.DespesasFixas).ToList();
            if (lDespesasFixas.Any())
            {
                var firstOrDefault = lDespesasFixas.GroupBy(x => x.DataLancamento.Month).Select(y => new { TotalDespFixas = y.Sum(i => i.Valor) }).FirstOrDefault();
                oDRE.TotalDespesasFixas = firstOrDefault != null ? firstOrDefault.TotalDespFixas : 0;
            }

            //LUCRO OPERACIONAL
            oDRE.LucroOperacional = oDRE.LucroBruto - oDRE.TotalDespesasVariaveis;

            //LUCRO LÍQUIDO DO EXERCÍCIO
            oDRE.LiquidoDoExercicio = oDRE.LucroOperacional - oDRE.TotalDespesasFixas;

            return oDRE;
        }
        public IList<LancamentoViewModel> LoadUltimosLancamentos()
        {
            IList<LancamentoViewModel> lUltLancamentos = new List<LancamentoViewModel>();

            var lancamentos = _appLancamento.ListarUltimos50LancamentosByUserId(GetUser);
            if (lancamentos.Any())
            {
                lancamentos.ForEach(x => lUltLancamentos.Add(new LancamentoViewModel
                {
                    Id = x.Id,
                    Descricao = x.Descricao,
                    DataLancamento = x.DataLancamento,
                    Valor = x.Valor,
                    DataRealizado = x.DataRealizado,
                    ContaOrigemId = x.ContaOrigemId,
                    ContaOrigemDescricao = _appConta.ListarPorId(x.ContaOrigemId).Descricao,
                    FormaPagtoDescricao = _appFormaPagto.ListarPorId(x.FormaPagtoId).Descricao,
                    CategoriaDescricao = _appCategoria.ListarPorId(x.CategoriaId).Descricao
                }));
            }
            return lUltLancamentos;
        }
        [HttpGet]
        public PartialViewResult LoadUltimosLancamentosView()
        {
            return PartialView("_LancamentoPartialList", LoadUltimosLancamentos());
        }
        public JsonResult LoadComparativoMesAnterior()
        {
            var comparativo = new ComparativoMesCorrenteAnteriorViewModel
            {
                MesAnterior = DateTime.Now.Month == 1 ? 12 : DateTime.Now.Month - 1,
            };

            comparativo.Meses.Add(string.Concat(DateTime.Now.Month, "/", DateTime.Now.Year)); //MM/yyyy Corrente
            comparativo.Meses.Add(string.Concat(DateTime.Now.Month == 1 ? 12 : DateTime.Now.Month - 1, "/", DateTime.Now.Year)); //MM/yyyy Anterior

            var lancamentoMesAtual = _appLancamento.ListarTodosRealizadosByUserIdAndMonth(GetUser, DateTime.Now.Month);
            var lancamentoMesAnterior = _appLancamento.ListarTodosRealizadosByUserIdAndMonth(GetUser, comparativo.MesAnterior);

            #region Mes Corrente

            var despFixasAtual = lancamentoMesAtual.Where(x => x.TipoAtividadeId == (int)GenericEnum.TipoAtividade.DespesasFixas).ToList();
            comparativo.TotalDespFixa.Add(!despFixasAtual.Any() ? 0 :
                despFixasAtual.GroupBy(x => x.DataRealizado.Value.Month).Select(y => new { TotalDespFixas = y.Sum(i => i.Valor) }).FirstOrDefault().TotalDespFixas);

            var despVariaveisAtual = lancamentoMesAtual.Where(x => x.TipoAtividadeId == (int)GenericEnum.TipoAtividade.DespesasVariaveis).ToList();
            comparativo.TotalDespVariavel.Add(!despVariaveisAtual.Any() ? 0 :
               despVariaveisAtual.GroupBy(x => x.DataRealizado.Value.Month).Select(y => new { TotalDespVariaveis = y.Sum(i => i.Valor) }).FirstOrDefault().TotalDespVariaveis);

            var impostosAtual = lancamentoMesAtual.Where(x => x.TipoAtividadeId == (int)GenericEnum.TipoAtividade.Impostos).ToList();
            comparativo.TotalImposto.Add(!impostosAtual.Any() ? 0 :
                impostosAtual.GroupBy(x => x.DataRealizado.Value.Month).Select(y => new { TotalImpostos = y.Sum(i => i.Valor) }).FirstOrDefault().TotalImpostos);

            var recebimentosAtual = lancamentoMesAtual.Where(x => x.TipoAtividadeId == (int)GenericEnum.TipoAtividade.Recebimentos).ToList();
            comparativo.TotalRecebimento.Add(!recebimentosAtual.Any() ? 0 :
                recebimentosAtual.GroupBy(x => x.DataRealizado.Value.Month).Select(y => new { TotalRecebimentos = y.Sum(i => i.Valor) }).FirstOrDefault().TotalRecebimentos);

            #endregion

            #region MesAnterior
            
            var despFixasAnterior = lancamentoMesAnterior.Where(x => x.TipoAtividadeId == (int)GenericEnum.TipoAtividade.DespesasFixas).ToList();
            comparativo.TotalDespFixa.Add(!despFixasAnterior.Any() ? 0 :
                despFixasAnterior.GroupBy(x => x.DataRealizado.Value.Month).Select(y => new { TotalDespFixas = y.Sum(i => i.Valor) }).FirstOrDefault().TotalDespFixas);

            var despVariaveisAnterior = lancamentoMesAnterior.Where(x => x.TipoAtividadeId == (int)GenericEnum.TipoAtividade.DespesasVariaveis).ToList();
            comparativo.TotalDespVariavel.Add(!despVariaveisAnterior.Any() ? 0 :
                despVariaveisAnterior.GroupBy(x => x.DataRealizado.Value.Month).Select(y => new { TotalDespVariaveis = y.Sum(i => i.Valor) }).FirstOrDefault().TotalDespVariaveis);

            var impostosAnterior = lancamentoMesAnterior.Where(x => x.TipoAtividadeId == (int)GenericEnum.TipoAtividade.Impostos).ToList();
            comparativo.TotalImposto.Add(!impostosAnterior.Any() ? 0 :
                impostosAnterior.GroupBy(x => x.DataRealizado.Value.Month).Select(y => new { TotalImpostos = y.Sum(i => i.Valor) }).FirstOrDefault().TotalImpostos);

            var recebimentosAnterior = _appLancamento.ListarTodosRealizadosByUserIdTipoAtivideIdMonth(GetUser, (int)GenericEnum.TipoAtividade.Recebimentos, comparativo.MesAnterior);
            comparativo.TotalRecebimento.Add(!recebimentosAnterior.Any() ? 0 :
                recebimentosAnterior.GroupBy(x => x.DataRealizado.Value.Month).Select(y => new { TotalRecebimentos = y.Sum(i => i.Valor) }).FirstOrDefault().TotalRecebimentos);
            
            #endregion

            return this.Json(comparativo, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult LoadRealizadoMes()
        {
            decimal totalRecebimentos;
            decimal totalDespFixas;
            decimal totalDespVariaveis;
            decimal totalImpostos;

            var realizados = new List<StringBuilder>();

            var categoriasDados = _appCategoria.ListarTodos().ToList();
            var lancamentos = _appLancamento.ListarTodosRealizadosByUserIdAndMonth(GetUser, DateTime.Now.Month);

            var recebimentos = lancamentos.Where(x => x.TipoAtividadeId == (int)GenericEnum.TipoAtividade.Recebimentos);
            var despFixas = lancamentos.Where(x => x.TipoAtividadeId == (int)GenericEnum.TipoAtividade.DespesasFixas);
            var despVariaveis = lancamentos.Where(x => x.TipoAtividadeId == (int)GenericEnum.TipoAtividade.DespesasVariaveis);
            var impostos = lancamentos.Where(x => x.TipoAtividadeId == (int)GenericEnum.TipoAtividade.Impostos);

            totalRecebimentos = recebimentos.Any() ? recebimentos.Sum(x => x.Valor) : 0;
            totalDespFixas = despFixas.Any() ? despFixas.Sum(x => x.Valor) : 0;
            totalDespVariaveis = despVariaveis.Any() ? despVariaveis.Sum(x => x.Valor) : 0;
            totalImpostos = impostos.Any() ? impostos.Sum(x => x.Valor) : 0;

            var dados = new List<ChartRealizados>();

            //recebimentos
            if (totalRecebimentos > 0)
                dados.Add(
                        new ChartRealizados
                        {
                            y = totalRecebimentos,
                            color = "colors[0]",
                            dados = new DrillDownRealizados
                            {
                                name = "Categorias de Recebimento",
                                categories = categoriasDados.Where(x => recebimentos.GroupBy(y => y.CategoriaId).Select(z => z.Key).Contains(x.Id)).Select(w => w.Descricao).ToList(),
                                data = recebimentos.GroupBy(x => x.CategoriaId).Select(y => y.Sum(z => z.Valor)).ToList(),
                                color = "0"
                            }
                        });

            //despesas fixas
            if (totalDespFixas > 0)
                dados.Add(
                    new ChartRealizados {
                        y = totalDespFixas,
                        color = "colors[1]",
                        dados = new DrillDownRealizados {
                            name = "Categorias de Despesas Fixas",
                            categories = categoriasDados.Where(x => despFixas.GroupBy(y => y.CategoriaId).Select(z => z.Key).Contains(x.Id)).Select(w => w.Descricao).ToList(),
                            data = despFixas.GroupBy(x => x.CategoriaId).Select(y => y.Sum(z => z.Valor)).ToList(),
                            color = "1"
                        }
                    });

            ////despesas variaveis
            if (totalDespVariaveis > 0)
                dados.Add(
                    new ChartRealizados {
                        y = totalDespVariaveis,
                        color = "colors[2]",
                        dados = new DrillDownRealizados
                        {
                            name = "Categorias de Despesas Variaveis",
                            categories = categoriasDados.Where(x => despVariaveis.GroupBy(y => y.CategoriaId).Select(z => z.Key).Contains(x.Id)).Select(w => w.Descricao).ToList(),
                            data = despVariaveis.GroupBy(x => x.CategoriaId).Select(y => y.Sum(z => z.Valor)).ToList(),
                            color = "2"
                        }});

            //impostos
            if (totalImpostos > 0)
                dados.Add(
                    new ChartRealizados
                    {
                        y = totalImpostos,
                        color = "colors[3]",
                        dados = new DrillDownRealizados
                        {
                            name = "Categorias de Impostos",
                            categories = categoriasDados.Where(x => impostos.GroupBy(y => y.CategoriaId).Select(z => z.Key).Contains(x.Id)).Select(w => w.Descricao).ToList(),
                            data = impostos.GroupBy(x => x.CategoriaId).Select(y => y.Sum(z => z.Valor)).ToList(),
                            color = "3"
                        }
                    });
            return Json(dados, JsonRequestBehavior.AllowGet);

        }

        public JsonResult LoadDesempenhoMes()
        {
            var chartDesempenho = new ChartDesempenho
            {DataRealizado = new List<DateTime>(), diaRealizado = new List<string>(), TotalRecebido = new List<decimal>(), TotalDespesas = new List<decimal>()};
            
            var lancamentos = _appLancamento.ListarTodosRealizadosByUserIdAndMonth(GetUser, DateTime.Now.Month);
            var recebimentos = lancamentos.Where(x => x.TipoAtividadeId == (int) GenericEnum.TipoAtividade.Recebimentos).ToList();
            var despesas = lancamentos.Where(x => x.TipoAtividadeId != (int) GenericEnum.TipoAtividade.Recebimentos).ToList();
            if (lancamentos.Any())
            {
                for (int i = 1; i <= DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month); i++)
                {
                    chartDesempenho.diaRealizado.Add(i.ToString());
                    chartDesempenho.DataRealizado.Add(new DateTime(DateTime.Now.Year, DateTime.Now.Month, i));

                    if (!recebimentos.Any())
                        chartDesempenho.TotalRecebido.Add(0);
                    else
                    {
                        var totalRecebidoByDay = recebimentos.Where(x => x.DataRealizado == chartDesempenho.DataRealizado[i - 1]);
                        if(totalRecebidoByDay.Any())
                            chartDesempenho.TotalRecebido.Add(totalRecebidoByDay.GroupBy(y => y.DataRealizado).Select(z => new {Valor = z.Sum(w => w.Valor)}).FirstOrDefault().Valor);
                        else
                            chartDesempenho.TotalRecebido.Add(0);
                    }

                    if (!despesas.Any())
                        chartDesempenho.TotalDespesas.Add(0);
                    else
                    {
                        var totalDespesaByDay = despesas.Where(x => x.DataRealizado == chartDesempenho.DataRealizado[i - 1]);
                        if (totalDespesaByDay.Any())
                            chartDesempenho.TotalDespesas.Add(totalDespesaByDay.GroupBy(y => y.DataRealizado).Select(z => new {Valor = z.Sum(w => w.Valor)}).FirstOrDefault().Valor);
                        else
                            chartDesempenho.TotalDespesas.Add(0);
                    }
                }
            }
            else
            {
                for (int i = 1; i <= DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month); i++)
                {
                    chartDesempenho.diaRealizado.Add(i.ToString());
                    chartDesempenho.DataRealizado.Add(new DateTime(DateTime.Now.Year, DateTime.Now.Month, i));
                    chartDesempenho.TotalRecebido.Add(0);
                    chartDesempenho.TotalDespesas.Add(0);
                }
            }

            return this.Json(chartDesempenho, JsonRequestBehavior.AllowGet);
        }
    }

    public class ChartDesempenho
    {
        public IList<DateTime> DataRealizado { get; set; }
        public IList<decimal> TotalRecebido { get; set; }
        public IList<decimal> TotalDespesas { get; set; }
        public IList<string> diaRealizado { get; set; }
    }

    public class MainChartRealizados
    {
        public IList<ChartRealizados> listaRealizados { get; set; }
    }

    public class ChartRealizados
    {
        public decimal y { get; set; } //total categoria
        public string color { get; set; }
        public DrillDownRealizados dados { get; set; }
    }

    public class DrillDownRealizados
    {
        public DrillDownRealizados()
        {
            categories = new List<string>();
            data = new List<decimal>();
        }

        public string name { get; set; }
        public IList<string> categories { get; set; }
        public IList<decimal> data { get; set; }
        public string color { get; set; }
    }
}