﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using sbAdminTCC.Aplicacao.App;
using sbAdminTCC.Aplicacao.Fabrica;
using sbAdminTCC.Dominio.Entidade;
using sbAdminTCC.UI.Web.Models;
using WebGrease.Css.Extensions;

namespace sbAdminTCC.UI.Web.Controllers
{
    public class LancamentoController : BaseController
    {
        private readonly FormaPagtoAplicacao _appFormaPagto;
        private readonly ContaAplicacao _appConta;
        private readonly LancamentoAplicacao _appLancamento;
        private readonly CategoriaAplicacao _appCategoria;

        public LancamentoController()
        {
            _appFormaPagto = FabricaAplicacao.GetNewFormaPagtoAplicacao();
            _appConta = FabricaAplicacao.GetNewContaAplicacao();
            _appLancamento = FabricaAplicacao.GetNewLancamentoAplicacao();
            _appCategoria = FabricaAplicacao.GetNewCategoriaAplicacao();
        }

        public ActionResult Index(int? tipoAtividadeId)
        {
            CarregaViewBag(tipoAtividadeId ?? 1);
            return View(GetLancamentoByTipo(tipoAtividadeId ?? 1));
        }

        [HttpPost]
        public PartialViewResult CarregaPartialLancamentos(int id)
        {
            return PartialView("_LancamentoPartialList", GetLancamentoByTipo(id));
        }

        public void CarregaViewBag(int? tipoAtividadeId)
        {
            var lForma = _appFormaPagto.ListarTodos();
            var lConta = _appConta.ListarTodosByUserId(GetUser);
            var lCategoria = _appCategoria.ListarTodosPorTipoAtividadeId(tipoAtividadeId ?? 1);

            ViewBag.ListarFormaPagto = new SelectList(lForma, "Id", "Descricao");
            ViewBag.ListarConta = new SelectList(lConta, "Id", "Descricao");
            ViewBag.ListarCategoria = new SelectList(lCategoria, "Id", "Descricao");
            ViewBag.tipoAtividadeId = (tipoAtividadeId ?? 1);
        }

        [HttpPost]
        public ActionResult Create(LancamentoViewModel model)
        {
            _appLancamento.Salvar(new Lancamento
            {
                ContaOrigemId = model.ContaOrigemId,
                DataLancamento = model.DataLancamento,
                DataRealizado = model.DataRealizado,
                Descricao = model.Descricao,
                FormaPagtoId = model.FormaPagtoId,
                Realizado = model.Realizado,
                TipoAtividadeId = model.TipoAtividadeId,
                Valor = model.Valor,
                CategoriaId = model.CategoriaId,
                UserId = GetUser
            });
            return RedirectToAction("Index", "Lancamento", new { tipoAtividadeId = model.TipoAtividadeId });
        }

        public IList<LancamentoViewModel> GetLancamentoByTipo(int tipoAtividade)
        {
            var lLancamento = new List<LancamentoViewModel>();
            _appLancamento.ListarTodosByUserIdAndTipoAtividadeId(GetUser, tipoAtividade).ForEach(x =>
                        lLancamento.Add(new LancamentoViewModel
                        {
                            Id = x.Id,
                            Descricao = x.Descricao,
                            DataLancamento = x.DataLancamento,
                            Valor = x.Valor,
                            DataRealizado = x.DataRealizado,
                            ContaOrigemId = x.ContaOrigemId,
                            ContaOrigemDescricao = _appConta.ListarPorId(x.ContaOrigemId).Descricao,
                            FormaPagtoDescricao = _appFormaPagto.ListarPorId(x.FormaPagtoId).Descricao,
                            CategoriaDescricao = _appCategoria.ListarPorId(x.CategoriaId).Descricao
                        }));
            return lLancamento;
        }

        [HttpPost]
        public ActionResult Delete(int id, int tpAtividadeId)
        {
            _appLancamento.Excluir(id);
            return RedirectToAction("Index", "Lancamento", new { tipoAtividadeId = tpAtividadeId });
        }

        public JsonResult GetLancamentoEdit(int id)
        {
            var lancamento = _appLancamento.ListarPorId(id);
            string dtRealizado = lancamento.Realizado ? ((DateTime) lancamento.DataRealizado).ToString("dd/MM/yyyy") : "";
            
            var lanc = new LancamentoViewModel()
            {
                Id = lancamento.Id,
                Descricao = lancamento.Descricao,
                ValorJson = lancamento.Valor.ToString().Replace(".",","),
                Realizado = lancamento.Realizado,
                TipoAtividadeId = lancamento.TipoAtividadeId,
                FormaPagtoId = lancamento.FormaPagtoId,
                ContaOrigemId = lancamento.ContaOrigemId,
                DataLancamentoJson = lancamento.DataLancamento.ToString("dd/MM/yyyy"),
                DataRealizadoJson = dtRealizado,
                CategoriaId = lancamento.CategoriaId,
            };
            return this.Json(lanc, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Edit(LancamentoViewModelEdit model)
        {
            var lancamento = _appLancamento.ListarPorId(model.IdEdit);
            if (lancamento != null)
            {
                lancamento.Descricao = model.DescricaoEdit;
                lancamento.Valor = model.ValorEdit;
                lancamento.FormaPagtoId = model.FormaPagtoIdEdit;
                lancamento.Realizado = model.RealizadoEdit;
                lancamento.TipoAtividadeId = model.TipoAtividadeIdEdit;
                lancamento.ContaOrigemId = model.ContaOrigemIdEdit;
                lancamento.DataLancamento = Convert.ToDateTime(model.DataLancamentoEdit);
                lancamento.DataRealizado = model.RealizadoEdit ? Convert.ToDateTime(model.DataRealizadoEdit) : (DateTime?) null;
                lancamento.CategoriaId = model.CategoriaIdEdit;
            }
            _appLancamento.Salvar(lancamento);

            return RedirectToAction("Index", "Lancamento", new { tipoAtividadeId = model.TipoAtividadeIdEdit });
        }

	}
}
