﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Services.Description;
using Microsoft.AspNet.Identity;
using sbAdminTCC.Aplicacao.App;
using sbAdminTCC.Aplicacao.Fabrica;
using sbAdminTCC.Dominio.Entidade;
using sbAdminTCC.UI.Web.Models;
using WebGrease.Css.Extensions;

namespace sbAdminTCC.UI.Web.Controllers
{
    public class ContaController : BaseController
    {
        private readonly ContaAplicacao _appConta;
        private readonly LancamentoAplicacao _appLancamento;
        private string userId { get; set; }

        public ContaController()
        {
            _appConta = FabricaAplicacao.GetNewContaAplicacao();
            _appLancamento = FabricaAplicacao.GetNewLancamentoAplicacao();
        }

        public ActionResult Index()
        {
            return View(this.ListarTodos());
        }

        [HttpPost]
        public ActionResult Create(ContaViewModel model)
        {
            _appConta.Salvar(new Conta { Descricao = model.Descricao, UserId = GetUser });
            return RedirectToAction("Index", "Conta");
        }

        [HttpPost]
        public ActionResult Edit(ContaViewModel model)
        {
            _appConta.Salvar(new Conta { Id = model.Id, Descricao = model.Descricao, UserId = GetUser });
            return RedirectToAction("Index", "Conta");
        }

        [HttpPost]
        public JsonResult Delete(int id)
        {
            try
            {
                if (_appLancamento.VerificaRelacionaentoContaId(id))
                    throw new Exception("Exclusão cancelada. Existem lançamentos utilizando esta conta.");

                _appConta.Excluir(id);
                return Json(new {success = true});
            }
            catch (Exception e)
            {
                return Json(new {success = false, response = e.Message});
            }
        }

        private IList<ContaViewModel> ListarTodos()
        {
            var lConta = new List<ContaViewModel>();
            _appConta.ListarTodosByUserId(GetUser).ForEach(x => lConta.Add(new ContaViewModel { Descricao = x.Descricao, Id = x.Id }));
            return lConta;
        }
	}
}