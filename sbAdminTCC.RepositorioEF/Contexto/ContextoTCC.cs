﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using sbAdminTCC.Dominio.Entidade;

namespace sbAdminTCC.RepositorioEF.Contexto
{
    public class ContextoTCC : DbContext
    {
        public ContextoTCC()
            : base("sbAdminTCC")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ContextoTCC, Migrations.Configuration>("sbAdminTCC"));
        }

        public DbSet<Conta> Contas { get; set; }
        public DbSet<FormaPagto> FormaPagtos { get; set; }
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<Lancamento> Lancamentos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }
    }
}
