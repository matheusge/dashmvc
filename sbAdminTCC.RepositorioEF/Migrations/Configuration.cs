using System;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using sbAdminTCC.Dominio.Entidade;
using sbAdminTCC.RepositorioEF.Contexto;
using sbAdminTCC.RepositorioEF.Repositorio;
using System.Data.Entity.Migrations;

namespace sbAdminTCC.RepositorioEF.Migrations
{
    
    using System.Collections.Generic;

    internal sealed class Configuration : DbMigrationsConfiguration<ContextoTCC>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(ContextoTCC context)
        {
            context.FormaPagtos.AddOrUpdate(x => x.Id,
                new FormaPagto() { Id = 1, Descricao = "Dinheiro" },
                new FormaPagto() { Id = 2, Descricao = "Cheque" },
                new FormaPagto() { Id = 3, Descricao = "Boleto Banc�rio" },
                new FormaPagto() { Id = 4, Descricao = "Cart�o Cr�dito" },
                new FormaPagto() { Id = 5, Descricao = "Cart�o D�bito" },
                new FormaPagto() { Id = 6, Descricao = "Transfer�ncia Banc�ria" },
                new FormaPagto() { Id = 7, Descricao = "Promiss�ria" },
                new FormaPagto() { Id = 8, Descricao = "D�bito Autom�tico" });

            #region SeedCategoria

            int tipoAtividadeId = 1;

            //Recebimentos
            context.Categorias.AddOrUpdate(
                new Categoria() { Id = 1, Descricao = "Adiantamento", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 2, Descricao = "Boleto", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 3, Descricao = "Cart�o", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 4, Descricao = "Cobran�a", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 5, Descricao = "Comiss�o", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 6, Descricao = "Dep�sito", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 7, Descricao = "Empr�stimo", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 8, Descricao = "Mensalidade", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 9, Descricao = "Rendimentos", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 10, Descricao = "Transferencia", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 11, Descricao = "Vendas", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 133, Descricao = "Sal�rio", TipoAtividadeId = tipoAtividadeId},
                new Categoria() { Id = 134, Descricao = "Outros", TipoAtividadeId = tipoAtividadeId});

            //Despesas Fixas
            tipoAtividadeId = 2;
            context.Categorias.AddOrUpdate(
                new Categoria() { Id = 12, Descricao = "13� Sal�rio", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 13, Descricao = "�gua e esgoto", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 14, Descricao = "Aluguel", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 15, Descricao = "Aquisi��o de equipamentos", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 16, Descricao = "Assessorias e Associa��es", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 17, Descricao = "Assist�ncia M�dica", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 18, Descricao = "Assist�ncia odontol�gica", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 19, Descricao = "Cart�rio", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 20, Descricao = "Combust�vel", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 21, Descricao = "Comiss�o de vendedores", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 22, Descricao = "Contabilidade", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 23, Descricao = "Empr�stimos", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 24, Descricao = "Energia el�trica", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 25, Descricao = "Horas Extras", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 26, Descricao = "Internet", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 27, Descricao = "Investimentos", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 28, Descricao = "Juros", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 29, Descricao = "Limpeza", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 30, Descricao = "Manuten��o de equipamentos", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 31, Descricao = "Material de escrit�rio", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 32, Descricao = "Passagem a�reas", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 33, Descricao = "Publicidade", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 34, Descricao = "Rescis�es trabalhistas", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 35, Descricao = "Taxas banc�rias", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 36, Descricao = "Telefone celular", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 37, Descricao = "Telefone fixo", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 38, Descricao = "Translado", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 39, Descricao = "Treinamentos", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 40, Descricao = "Vale Alimenta��o", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 41, Descricao = "Vale Transporte", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 42, Descricao = "Viagens", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 135, Descricao = "Outros", TipoAtividadeId = tipoAtividadeId});

            //Despesas Vari�veis
            tipoAtividadeId = 3;
            context.Categorias.AddOrUpdate(
                new Categoria() { Id = 43, Descricao = "13� Sal�rio", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 44, Descricao = "�gua e esgoto", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 45, Descricao = "Aluguel", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 46, Descricao = "Aquisi��o de equipamentos", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 47, Descricao = "Assessorias e Associa��es", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 48, Descricao = "Assist�ncia M�dica", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 49, Descricao = "Assist�ncia odontol�gica", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 50, Descricao = "Cart�rio", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 51, Descricao = "Combust�vel", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 52, Descricao = "Comiss�o de vendedores", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 53, Descricao = "Contabilidade", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 54, Descricao = "Empr�stimos", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 55, Descricao = "Energia el�trica", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 56, Descricao = "Horas Extras", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 57, Descricao = "Internet", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 58, Descricao = "Investimentos", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 59, Descricao = "Juros", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 60, Descricao = "Limpeza", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 61, Descricao = "Manuten��o de equipamentos", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 62, Descricao = "Material de escrit�rio", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 63, Descricao = "Passagem a�reas", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 64, Descricao = "Publicidade", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 65, Descricao = "Rescis�es trabalhistas", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 66, Descricao = "Taxas banc�rias", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 67, Descricao = "Telefone celular", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 68, Descricao = "Telefone fixo", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 69, Descricao = "Translado", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 70, Descricao = "Treinamentos", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 71, Descricao = "Vale Alimenta��o", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 72, Descricao = "Vale Transporte", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 73, Descricao = "Viagens", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 136, Descricao = "Outros", TipoAtividadeId = tipoAtividadeId });

            //Impostos
            tipoAtividadeId = 4;
            context.Categorias.AddOrUpdate(
                new Categoria() { Id = 74, Descricao = "Alvar�", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 75, Descricao = "Cofins", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 76, Descricao = "CSLL", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 77, Descricao = "FGTS", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 78, Descricao = "GPS", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 79, Descricao = "ICMS", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 80, Descricao = "Imposto de Renda", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 81, Descricao = "INSS", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 82, Descricao = "IOF", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 83, Descricao = "IPI", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 84, Descricao = "IPTU", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 85, Descricao = "IPVA", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 86, Descricao = "IR", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 87, Descricao = "IRPJ", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 88, Descricao = "ISS", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 89, Descricao = "Juros", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 90, Descricao = "PIS", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 91, Descricao = "Simples Nacional", TipoAtividadeId = tipoAtividadeId },
                new Categoria() { Id = 137, Descricao = "Outros", TipoAtividadeId = tipoAtividadeId });

            #endregion

            context.SaveChanges();
        }
    }
}


