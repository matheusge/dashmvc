﻿using System;
using System.Collections.Generic;
using sbAdminTCC.Dominio.Entidade;
using sbAdminTCC.RepositorioEF.Base;

namespace sbAdminTCC.Aplicacao.App
{
    public class FormaPagtoAplicacao
    {
        private readonly IRepositorio<FormaPagto> _repositorio;

        public FormaPagtoAplicacao(IRepositorio<FormaPagto> repositorio)
        {
            _repositorio = repositorio;
        }

        public IEnumerable<FormaPagto> ListarTodos()
        {
            try
            {
                return _repositorio.GetAll();
            }
            catch (Exception e)
            {
                throw new Exception("Erro ListarTodos()", e.InnerException);
            }
        }

        public FormaPagto ListarPorId(int id)
        {
            try
            {
                return _repositorio.Find(id);
            }
            catch (Exception e)
            {
                throw new Exception("Erro Find()", e.InnerException);
            }
        }
    }
}
