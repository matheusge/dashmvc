﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using sbAdminTCC.Dominio.Entidade;
using sbAdminTCC.RepositorioEF.Base;
using sbAdminTCC.Fabrica;

namespace sbAdminTCC.Aplicacao.App
{
    public class LancamentoAplicacao
    {
        private readonly Repositorio<Lancamento> _repositorio;

        public LancamentoAplicacao(Repositorio<Lancamento> repositorio)
        {
            _repositorio = repositorio;
        }
        public IEnumerable<Lancamento> ListarTodosByUserIdAndTipoAtividadeId(string userId, int tipoAtividadeId)
        {
            try
            {
                return _repositorio.Get(x => x.TipoAtividadeId == tipoAtividadeId && x.UserId == userId);
            }
            catch (Exception e)
            {
                throw new Exception("Erro ListarTodosByUserIdAndTipoAtividadeId()", e.InnerException);
            }
        }
        public IEnumerable<Lancamento> ListarUltimos50LancamentosByUserId(string userId)
        {
            try
            {
                return _repositorio.GetAll().OrderByDescending(x => x.Id).Take(50).Where(x => x.UserId == userId);
            }
            catch (Exception e)
            {
                throw new Exception("Erro ListarUltimos10Lancamentos()", e.InnerException);
            }
        }
        public IEnumerable<Lancamento> ListarTodosRealizadosByUserIdTipoAtivideIdMonth(string userId, int tipoAtividadeId, int month)
        {
            try
            {
                return _repositorio.Get(x => x.DataRealizado != null 
                    && (x.Realizado == true && x.TipoAtividadeId == tipoAtividadeId && x.DataRealizado.Value.Month == month) && x.UserId == userId);
            }
            catch (Exception e)
            {
                throw new Exception("Erro ListarTodosRealizadosByUserIdTipoAtivideIdMonth()", e.InnerException);
            }
        }
        public IEnumerable<Lancamento> ListarTodosRealizadosByUserIdDespesasAndMonth(string userId, int month)
        {
            try
            {
                return _repositorio.Get(x => x.DataRealizado != null
                                             &&
                                             (x.Realizado == true && x.TipoAtividadeId != 1 &&
                                              x.DataRealizado.Value.Month == month) && x.UserId == userId);
            }
            catch (Exception e)
            {
                throw new Exception("Erro ListarTodosRealizadosByUserIdDespesasAndMonth()", e.InnerException);
            }
        }
        public IEnumerable<Lancamento> ListarTodosRealizadosByUserIdAndMonth(string userId, int month)
        {
            try
            {
                return _repositorio.Get(x => x.DataRealizado != null && (x.Realizado == true && x.DataRealizado.Value.Month == month) && x.UserId == userId);
            }
            catch (Exception e)
            {
                throw new Exception("Erro ListarTodosRealizadosByUserIdDespesasAndMonth()", e.InnerException);
            }
        }
        public Lancamento ListarPorId(int id)
        {
            try
            {
                return _repositorio.Find(id);
            }
            catch (Exception e)
            {
                throw new Exception("Erro Find()", e.InnerException);
            }
        }
        public bool VerificaRelacionaentoContaId(int id)
        {
            try
            {
                var lanc = _repositorio.Get(x => x.ContaOrigemId == id);
                if (lanc.Any())
                    return true;
                return false;
            }
            catch (Exception e)
            {
                throw new Exception("Erro Find()", e.InnerException);
            }
        }
        public void Salvar(Lancamento obj)
        {
            try
            {
                var lancamento = _repositorio.Find(obj.Id);
                if (lancamento != null)
                {
                    lancamento.Descricao = obj.Descricao;
                    lancamento.Valor = obj.Valor;
                    lancamento.DataLancamento = obj.DataLancamento;
                    lancamento.Realizado = obj.Realizado;
                    lancamento.DataRealizado = obj.DataRealizado;
                    lancamento.ContaOrigemId = obj.ContaOrigemId;
                    lancamento.FormaPagtoId = obj.FormaPagtoId;
                    lancamento.TipoAtividadeId = obj.TipoAtividadeId;
                    lancamento.UserId = obj.UserId;

                    _repositorio.Atualizar(lancamento);
                }
                else
                    _repositorio.Adicionar(obj);

                _repositorio.SalvarTodos();
            }
            catch (Exception e)
            {
                throw new Exception("Erro Salvar()", e.InnerException);
            }
        }
        public void Excluir(int id)
        {
            try
            {
                _repositorio.Excluir(x => x.Id.Equals(id));
                _repositorio.SalvarTodos();
            }
            catch (Exception e)
            {
                throw new Exception("Erro Excluir()", e.InnerException);
            }
        }
    }
}
