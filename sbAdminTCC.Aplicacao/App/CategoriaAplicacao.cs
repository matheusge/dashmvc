﻿using System;
using System.Collections;
using System.Collections.Generic;
using sbAdminTCC.Dominio.Entidade;
using sbAdminTCC.RepositorioEF.Base;

namespace sbAdminTCC.Aplicacao.App
{
    public class CategoriaAplicacao
    {
        private readonly IRepositorio<Categoria> _repositorio;

        public CategoriaAplicacao(IRepositorio<Categoria> repositorio)
        {
            _repositorio = repositorio;
        }

        public IEnumerable<Categoria> ListarTodos()
        {
            try
            {
                return _repositorio.GetAll();
            }
            catch (Exception e)
            {
                throw new Exception("Erro ListarTodos()", e.InnerException);
            }
        }

        public Categoria ListarPorId(int id)
        {
            try
            {
                return _repositorio.Find(id);
            }
            catch (Exception e)
            {
                throw new Exception("Erro Find()", e.InnerException);
            }
        }

        public IEnumerable<Categoria> ListarTodosPorTipoAtividadeId(int tipoAtividadeId)
        {
            try
            {
                return _repositorio.Get(x => x.TipoAtividadeId == tipoAtividadeId);
            }
            catch (Exception e)
            {
                throw new Exception("Erro ListarTodosPorTipoAtividadeId()", e.InnerException);
            }
        }

        public void Salvar(Categoria obj)
        {
            try
            {
                var categoria = _repositorio.Find(obj.Id);
                if (categoria != null)
                {
                    categoria.TipoAtividadeId = obj.TipoAtividadeId;
                    categoria.Descricao = obj.Descricao;
                    _repositorio.Atualizar(categoria);
                }
                else
                    _repositorio.Adicionar(obj);

                _repositorio.SalvarTodos();
            }
            catch (Exception e)
            {
                throw new Exception("Erro Salvar()", e.InnerException);
            }
        }

        public void Excluir(int id)
        {
            try
            {
                _repositorio.Excluir(x => x.Id.Equals(id));
            }
            catch (Exception e)
            {
                throw new Exception("Erro Excluir()", e.InnerException);
            }
        }
    }
}
