﻿using System;
using System.Collections.Generic;
using System.Linq;
using sbAdminTCC.Dominio.Entidade;
using sbAdminTCC.RepositorioEF.Base;

namespace sbAdminTCC.Aplicacao.App
{
    public class ContaAplicacao
    {
        private readonly IRepositorio<Conta> _repositorio;

        public ContaAplicacao(IRepositorio<Conta> repositorio)
        {
            _repositorio = repositorio;
        }

        public IEnumerable<Conta> ListarTodos()
        {
            try
            {
                return _repositorio.GetAll();
            }
            catch (Exception e)
            {
                throw new Exception("Erro ListarTodos()", e.InnerException);
            }
        }

        public IEnumerable<Conta> ListarTodosByUserId(string userId)
        {
            try
            {
                return _repositorio.Get(x => x.UserId == userId);
            }
            catch (Exception e)
            {
                throw new Exception("Erro ListarTodosByUserId() ", e.InnerException);
            }
        } 

        public Conta ListarPorId(int id)
        {
            try
            {
                return _repositorio.Find(id);
            }
            catch (Exception e)
            {
                throw new Exception("Erro Find()", e.InnerException);
            }
        }

        public void Salvar(Conta obj)
        {
            try
            {
                var conta = _repositorio.Find(obj.Id);
                if (conta != null)
                {
                    conta.Descricao = obj.Descricao;
                    conta.UserId = obj.UserId;
                    _repositorio.Atualizar(conta);
                }
                else
                    _repositorio.Adicionar(obj);

                _repositorio.SalvarTodos();
            }
            catch (Exception e)
            {
                throw new Exception("Erro Salvar()", e.InnerException);
            }
        }

        public void Excluir(int id)
        {
            try
            {
                _repositorio.Excluir(x => x.Id.Equals(id));
                _repositorio.SalvarTodos();
            }
            catch (Exception e)
            {
                throw new Exception("Erro Excluir()", e.InnerException);
            }
        }
    }
}
