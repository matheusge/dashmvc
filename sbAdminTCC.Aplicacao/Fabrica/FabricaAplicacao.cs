﻿using sbAdminTCC.Fabrica;
using sbAdminTCC.Aplicacao.App;

namespace sbAdminTCC.Aplicacao.Fabrica
{
    public class FabricaAplicacao
    {
        public static ContaAplicacao GetNewContaAplicacao()
        {
            return new ContaAplicacao(FabricaRepositorio.GetNewContaRepositorio());
        }

        public static CategoriaAplicacao GetNewCategoriaAplicacao()
        {
            return new CategoriaAplicacao(FabricaRepositorio.GetNewCategoriaRepositorio());
        }

        public static FormaPagtoAplicacao GetNewFormaPagtoAplicacao()
        {
            return new FormaPagtoAplicacao(FabricaRepositorio.GetNewFormaPagtoRepositorio());
        }

        public static LancamentoAplicacao GetNewLancamentoAplicacao()
        {
            return new LancamentoAplicacao(FabricaRepositorio.GetNewLancamentoRepositorio());
        }
    }
}
