﻿using sbAdminTCC.RepositorioEF.Repositorio;

namespace sbAdminTCC.Fabrica
{
    public class FabricaRepositorio
    {
        public static ContaRepositorio GetNewContaRepositorio()
        {
            return new ContaRepositorio();
        }

        public static CategoriaRepositorio GetNewCategoriaRepositorio()
        {
            return new CategoriaRepositorio();
        }

        public static FormaPagtoRepositorio GetNewFormaPagtoRepositorio()
        {
            return new FormaPagtoRepositorio();
        }

        public static LancamentoRepositorio GetNewLancamentoRepositorio()
        {
            return new LancamentoRepositorio();
        }
    }
}
