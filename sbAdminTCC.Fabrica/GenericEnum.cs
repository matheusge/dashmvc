﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sbAdminTCC.Fabrica
{
    public static class GenericEnum
    {
        public enum TipoAtividade : int { Recebimentos = 1, DespesasFixas = 2, DespesasVariaveis = 3, Impostos = 4, Transferências = 5 }
    }
}
