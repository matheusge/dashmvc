﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using sbAdminTCC.Dominio.Entidade;

namespace sbAdminTCC.Fabrica
{
    public class FabricaEntidade
    {
        public static Conta GetNewConta()
        {
            return new Conta();
        }

        public static Categoria GetNewCategoria()
        {
            return new Categoria();
        }

        public static FormaPagto GetNewFormaPagto()
        {
            return new FormaPagto();
        }

        public static Lancamento GetNewLancamento()
        {
            return new Lancamento();
        }
    }
}
